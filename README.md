![made-with-Cpp](https://img.shields.io/badge/Made%20with-C++-1f425f.svg?style=flat-square)
<!-- ![Actions Status](https://github.com/pinho/crossover-research/workflows/C++%20CI/badge.svg) -->

# Crossovers Operators Comparisons

This project is part of a research performed in the [Laboratory of Applied Artificial Intelligence (LAAI)](http://laai.ufpa.br) at Federal University of Pará (Brazil). Here, somo crossover operators of genetic algorihm are studied applying it to two classic combinatorial optimization problems, are both Karp's NP-Complete problems. The study aims to know more about the effect of crossover operators most used and known in genetic algorithms with binary encoding. Uniform, One-point and Many-points crossovers are compared.

The problems used in experiments for this study are:
* Set Covering problem;
* Steiner Tree problem.

Both problemas were modelled and executed using instances provided by [OR-Library](http://people.brunel.ac.uk/~mastjjb/jeb/info.html).

# How to use this repository

This repository contains the source code of the project with the implementations of GA for both problems and the instance files downloaded from OR-Library. The goal of this repository is let that anyone can reproduce the experiments performed.

# Build the project

All project implementations are made in C++ using some external libraries for specific use cases (Graphs, Database, CLI, Metaheuristic, etc). Also, we used [CMake](https://cmake.org) to automate the build configuration. Therefore, the process to configure and build this project in your local machine is composed by some steps, following.

## Installing required dependencies and tools

Arch Linux (and based):
```sh
$ sudo pacman -Sy --ignore wget unzip cmake docker doxygen boost boost-libs vsqlite++
```

Debian (and based):

```sh
$ sudo apt-get update
$ sudo apt-get install wget unzip cmake docker doxygen libboost-graph-dev libvsqlitepp-dev
```

## Install ParadisEO

ParadisEO is a framework for metaheuristic development in C++. A script is supplied for automate the installation on Linux. Run:

```sh
$ sudo chmod +x scripts/install-paradiseo.sh
$ ./scripts/install-paradiseo.sh
```

## Build the binaries

Use `cmake` to configure the Makefiles.

```sh
$ cmake -S . -B build
```

And, for build.

```sh
$ cmake --build build --target install
```

The executables will were placed in the `<path/of/the/project>/bin` directory.